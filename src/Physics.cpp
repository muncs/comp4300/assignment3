#include "Physics.h"
#include "Components.h"

Vec2 Physics::GetOverlap(std::shared_ptr<Entity> a, std::shared_ptr<Entity> b)
{
	const float x1 = a->getComponent<CTransform>()->pos.x;
	const float x2 = b->getComponent<CTransform>()->pos.x;

	const float y1 = a->getComponent<CTransform>()->pos.y;
	const float y2 = b->getComponent<CTransform>()->pos.y;

	const float w1 = a->getComponent<CBoundingBox>()->size.x;
	const float h1 = a->getComponent<CBoundingBox>()->size.y;

	const float w2 = b->getComponent<CBoundingBox>()->size.x;
	const float h2 = b->getComponent<CBoundingBox>()->size.y;

	Vec2 delta = Vec2(abs(x1 - x2), abs(y1 - y2));

	const float ox = (w1 / 2) + (w2 / 2) - delta.x;
	const float oy = (h1 / 2) + (h2 / 2) - delta.y;

    return Vec2(ox, oy);
}

Vec2 Physics::GetPreviousOverlap(std::shared_ptr<Entity> a, std::shared_ptr<Entity> b)
{
	const float x1 = a->getComponent<CTransform>()->prevPos.x;
	const float x2 = b->getComponent<CTransform>()->prevPos.x;

	const float y1 = a->getComponent<CTransform>()->prevPos.y;
	const float y2 = b->getComponent<CTransform>()->prevPos.y;

	const float w1 = a->getComponent<CBoundingBox>()->size.x;
	const float h1 = a->getComponent<CBoundingBox>()->size.y;

	const float w2 = b->getComponent<CBoundingBox>()->size.x;
	const float h2 = b->getComponent<CBoundingBox>()->size.y;

	Vec2 delta = Vec2(abs(x1 - x2), abs(y1 - y2));

	auto ox = a->getComponent<CBoundingBox>()->halfSize.x + b->getComponent<CBoundingBox>()->halfSize.x - delta.x;
	auto oy = a->getComponent<CBoundingBox>()->halfSize.y + b->getComponent<CBoundingBox>()->halfSize.y - delta.y;

	return Vec2(ox, oy);
}
