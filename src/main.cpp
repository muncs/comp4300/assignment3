/**
 * COMP 4300 - Assignment 3
 *
 * Created by:
 *      - Devin Marsh - 201239464
 *      - Justin Delaney - 201222684
 *
 * For an extra feature we added a score component that adds 100 score for each question box hit or "coin tile" collected.
 * For the coin tile, in our level 2 we made coin tiles placed around the map the player to "collect" when collided with, adding to the player's score.
 * 
 */



#include <SFML/Graphics.hpp>

#include "GameEngine.h"

int main()
{
    GameEngine g("assets.txt");
    g.run();
}