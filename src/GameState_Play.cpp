#include <cmath>
#include "GameState_Play.h"
#include "Common.h"
#include "Physics.h"
#include "Assets.h"
#include "GameEngine.h"
#include "Components.h"

GameState_Play::GameState_Play(GameEngine & game, const std::string & levelPath)
    : GameState(game)
    , m_levelPath(levelPath)
{
    init(m_levelPath);
}

void GameState_Play::init(const std::string & levelPath)
{
    loadLevel(levelPath);
    spawnPlayer();
}

void GameState_Play::loadLevel(const std::string & filename)
{
    //Reset the entity manager every time we load a level
    m_entityManager = EntityManager();

	//Read in level config file
	std::ifstream fin(m_levelPath);
	std::string token;

	while (fin.good())
	{
		fin >> token;

		if (token == "Tile")
		{
			std::string tilename;
			float t_X, t_Y;

			fin >> tilename >> t_X >> t_Y;

			auto tiles = m_entityManager.addEntity("tile");

			tiles->addComponent<CTransform>(Vec2(t_X, t_Y));
			tiles->addComponent<CAnimation>(m_game.getAssets().getAnimation(tilename), true);
			tiles->addComponent<CBoundingBox>(m_game.getAssets().getAnimation(tilename).getSize());
			
			//Add score to question or coin tiles
			if (tilename == "Question" || tilename == "Coin")
			{
				tiles->addComponent<CScore>(100);
			}
		}

		else if (token == "Dec")
		{
			std::string tilename;
			float d_X, d_Y;

			fin >> tilename >> d_X >> d_Y;

			auto decs = m_entityManager.addEntity("dec");

			decs->addComponent<CTransform>(Vec2(d_X, d_Y));
			decs->addComponent<CAnimation>(m_game.getAssets().getAnimation(tilename), true);
		}

		else if (token == "Player")
		{
			std::string B;
			float X, Y, CW, CH, SX, SY, SM, GY;

			fin >> X >> Y >> CW >> CH >> SX >> SY >> SM >> GY >> B;

			m_playerConfig.X = X;
			m_playerConfig.Y = Y;
			m_playerConfig.CX = CW;
			m_playerConfig.CY = CH;
			m_playerConfig.SPEED = SX;
			m_playerConfig.JUMP = SY;
			m_playerConfig.MAXSPEED = SM;
			m_playerConfig.GRAVITY = GY;
			m_playerConfig.WEAPON = B;
		}
		else 
		{
			std::cout << "Bad!";
		}
	}
	
	//Initialize score text
	m_scoreText.setFont(m_game.getAssets().getFont("Megaman"));
	m_scoreText.setColor(sf::Color(255, 0, 0));
	m_scoreText.setCharacterSize(20);
}

void GameState_Play::spawnPlayer()
{
    m_player = m_entityManager.addEntity("player");
    m_player->addComponent<CTransform>(Vec2(m_playerConfig.X, m_playerConfig.Y));
    m_player->addComponent<CBoundingBox>(Vec2(m_playerConfig.CX, m_playerConfig.CY));
    m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("Stand"), true);
	m_player->addComponent<CInput>();
	m_player->addComponent<CState>("standing");
	m_player->addComponent<CGravity>(m_playerConfig.GRAVITY);
	m_player->addComponent<CScore>(0);
}

void GameState_Play::spawnBullet(std::shared_ptr<Entity> entity)
{
    auto bullet = m_entityManager.addEntity("bullet");
	auto direction = m_player->getComponent<CTransform>()->scale.x;

	//Determine direction to fire bullet
	direction = direction / abs(direction);

    bullet->addComponent<CTransform>(
            Vec2(entity->getComponent<CTransform>()->pos.x, entity->getComponent<CTransform>()->pos.y),
            Vec2(direction * 10, 0),
            Vec2(1, 1),
            0.f);

    bullet->addComponent<CBoundingBox>(Vec2(20, 20));
    bullet->addComponent<CAnimation>(m_game.getAssets().getAnimation(m_playerConfig.WEAPON), true);
	bullet->addComponent<CLifeSpan>(1000);
}

void GameState_Play::update()
{
    m_entityManager.update();

	if (m_paused)
	{
		sUserInput();
	}
	else {
		sMovement();
		sLifespan();
		sCollision();
		sAnimation();
		sUserInput();
		sRender();
	}
}

void GameState_Play::sMovement()
{
	//Reset speed every frame
	m_player->getComponent<CTransform>()->speed.x = 0;

	//Player movement
	if (m_player->hasComponent<CTransform>()) 
	{
		//Handle Y axis
		if (m_player->getComponent<CInput>()->up && m_player->getComponent<CState>()->state != "jumping")
		{
			m_player->getComponent<CTransform>()->speed.y += m_playerConfig.JUMP;
			m_player->getComponent<CState>()->state = "jumping";
		}

		//Handle X axis
		if (m_player->getComponent<CInput>()->left && !m_player->getComponent<CInput>()->right)
		{
			m_player->getComponent<CTransform>()->scale = (Vec2(-1.0, 1.0));
			m_player->getComponent<CTransform>()->speed.x -= m_playerConfig.SPEED;
		}
		else if (m_player->getComponent<CInput>()->right && !m_player->getComponent<CInput>()->left)
		{
			m_player->getComponent<CTransform>()->scale = (Vec2(1.0, 1.0));
			m_player->getComponent<CTransform>()->speed.x += m_playerConfig.SPEED;
		}

		//Update Y position with gravity
		if (m_player->getComponent<CGravity>())
		{
			m_player->getComponent<CTransform>()->speed.y += m_player->getComponent<CGravity>()->gravity;
		}

		//Limit player speed
		m_player->getComponent<CTransform>()->speed.x = fmin(m_playerConfig.MAXSPEED, fmax(m_player->getComponent<CTransform>()->speed.x, -m_playerConfig.MAXSPEED));
		m_player->getComponent<CTransform>()->speed.y = fmin(m_playerConfig.MAXSPEED, fmax(m_player->getComponent<CTransform>()->speed.y, -m_playerConfig.MAXSPEED));
	}

	//Update positions for all entities
	for (auto entity : m_entityManager.getEntities())
	{
		if (entity->hasComponent<CTransform>())
		{
			entity->getComponent<CTransform>()->prevPos = entity->getComponent<CTransform>()->pos;
			entity->getComponent<CTransform>()->pos += entity->getComponent<CTransform>()->speed;
		}
	}
}

void GameState_Play::sLifespan()
{
	for (auto & entity : m_entityManager.getEntities()) {
		if (entity->hasComponent<CLifeSpan>() && !m_paused)
		{
			const float ratio = entity->getComponent<CLifeSpan>()->clock.getElapsedTime().asMilliseconds() / (float)entity->getComponent<CLifeSpan>()->lifespan;

			if (ratio >= 1.0)
			{
				entity->destroy();
			}
		}
	}
}

void GameState_Play::sCollision()
{
	//Player vs window
	if (m_player->getComponent<CTransform>()->pos.y < 0) {
		//Respawn player
		m_player->destroy();
		spawnPlayer();
	}

	//Prevent player from leaving screen on left side
	if (m_player->getComponent<CTransform>()->pos.x - m_player->getComponent<CBoundingBox>()->halfSize.x < 0)
	{
		m_player->getComponent<CTransform>()->pos.x = m_player->getComponent<CBoundingBox>()->halfSize.x;
	}

	//Player vs tile collisons
	for (auto & tile : m_entityManager.getEntities("tile")) {
		Vec2 overlap = Physics::GetOverlap(m_player, tile);
		Vec2 prevOverlap = Physics::GetPreviousOverlap(m_player, tile);

		const auto tileType = tile->getComponent<CAnimation>()->animation.getName();
		const auto tileTransform = tile->getComponent<CTransform>();
		const auto playerTransform = m_player->getComponent<CTransform>();
		auto playerState = m_player->getComponent<CState>();

		//Collision happens if theres an overlap in both axis
		if (overlap.x > 0 && overlap.y > 0) {
			//Player vs tile top
			if (prevOverlap.x > 0 && tileTransform->prevPos.y < playerTransform->prevPos.y)
			{	
				playerTransform->pos.y += overlap.y;
				playerTransform->speed.y = 0;
				
				if (playerTransform->speed.x == 0)
				{
					playerState->state = "standing";
				}
				else if (playerTransform->speed.x != 0)
				{
					playerState->state = "running";
				}

				//Collect coin
				if (tileType == "Coin")
				{
					m_player->getComponent<CScore>()->score += tile->getComponent<CScore>()->score;
					tile->destroy();
				}

				// Respawn player if you win
				if (tileType == "Pole" || tileType == "PoleTop")
				{
					m_player->destroy();
					m_game.popState();
				}
			}
			//Player vs tile bottom
			else if (prevOverlap.x > 0 && tileTransform->prevPos.y > playerTransform->prevPos.y)
			{
				playerTransform->pos.y -= overlap.y;
				playerTransform->speed.y = 0; 
				
				//Add a coin above the question tile if hit
				if (tileType == "Question")
				{
					tile->addComponent<CAnimation>(m_game.getAssets().getAnimation("Question2"), true);

					auto coin = m_entityManager.addEntity("dec");

					//Position coin above tile
					Vec2 coinPos(tile->getComponent<CTransform>()->pos);
					coinPos.y += tile->getComponent<CBoundingBox>()->size.y;

					coin->addComponent<CTransform>(coinPos);
					coin->addComponent<CAnimation>(m_game.getAssets().getAnimation("Coin"), true);
					coin->addComponent<CLifeSpan>(500);

					//Add score to player
					m_player->getComponent<CScore>()->score += tile->getComponent<CScore>()->score;
				}

				//Explode brick
				else if (tileType == "Brick")
				{
					tile->destroy();

					auto boom = m_entityManager.addEntity("dec");

					boom->addComponent<CTransform>()->pos = tile->getComponent<CTransform>()->pos;
					boom->addComponent<CAnimation>(m_game.getAssets().getAnimation("Explosion"), false);
				}

				//Collect coin
				if (tileType == "Coin")
				{
					m_player->getComponent<CScore>()->score += tile->getComponent<CScore>()->score;
					tile->destroy();
				}

				// Respawn player if you win
				if (tileType == "Pole" || tileType == "PoleTop")
				{
					m_player->destroy();
					m_game.popState();
				}
			}

			//Player vs tile left
			else if (prevOverlap.y > 0 && tileTransform->prevPos.x < playerTransform->prevPos.x)
			{
				playerTransform->pos.x += overlap.x;
				
				//Collect coin
				if (tileType == "Coin")
				{
					m_player->getComponent<CScore>()->score += tile->getComponent<CScore>()->score;
					tile->destroy();
				}

				// Respawn player if you win
				if (tileType == "Pole" || tileType == "PoleTop")
				{
					m_player->destroy();
					m_game.popState();
				}
			}

			//Player vs tile right
			else if (prevOverlap.y > 0 && tileTransform->prevPos.x > playerTransform->prevPos.x)
			{
				playerTransform->pos.x -= overlap.x;
				
				//Collect coin
				if (tileType == "Coin")
				{
					m_player->getComponent<CScore>()->score += tile->getComponent<CScore>()->score;
					tile->destroy();
				}

				// Respawn player if you win
				if (tileType == "Pole" || tileType == "PoleTop")
				{
					m_player->destroy();
					m_game.popState();
				}
			}
		}
	}

	//Bullets vs tiles
	for (auto & bullet : m_entityManager.getEntities("bullet")) {
		for (auto & tile : m_entityManager.getEntities("tile")) {
			Vec2 overlap = Physics::GetOverlap(bullet, tile);

			//Collision happens if theres an overlap in both axis
			if (overlap.x > 0 && overlap.y > 0) {
				const auto tileType = tile->getComponent<CAnimation>()->animation.getName();

				//Add explosion if brick
				if (tileType == "Brick") {
					tile->addComponent<CAnimation>(m_game.getAssets().getAnimation("Explosion"), false);
				}

				//Destroy bullet on every collision
				bullet->destroy();
			}
		}
	}

}

void GameState_Play::sUserInput()
{
    sf::Event event;
    while (m_game.window().pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_game.quit();
        }

        if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Escape:  { m_game.popState(); break; }
                case sf::Keyboard::Z:       { init(m_levelPath); break; }
                case sf::Keyboard::R:       { m_drawTextures = !m_drawTextures; break; }
                case sf::Keyboard::F:       { m_drawCollision = !m_drawCollision; break; }
                case sf::Keyboard::P:       { setPaused(!m_paused); break; }
				case sf::Keyboard::W:		{ m_player->getComponent<CInput>()->up = true;  break;  }
				case sf::Keyboard::S:		{ m_player->getComponent<CInput>()->down = true; break;	 }
				case sf::Keyboard::D:		{ m_player->getComponent<CInput>()->right = true; break; }
				case sf::Keyboard::A:		{ m_player->getComponent<CInput>()->left = true; break; }
				case sf::Keyboard::Space:   { spawnBullet(m_player);}
            }
        }

        if (event.type == sf::Event::KeyReleased)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Escape: { break;
				case sf::Keyboard::W: { m_player->getComponent<CInput>()->up = false;   break;   }
				case sf::Keyboard::S: { m_player->getComponent<CInput>()->down = false; break;	 }
				case sf::Keyboard::D: { m_player->getComponent<CInput>()->right = false; break; }
				case sf::Keyboard::A: { m_player->getComponent<CInput>()->left = false; break; }}
				
            }
        }
    }
}

void GameState_Play::sAnimation()
{
	for (auto entity : m_entityManager.getEntities()) {
		if (!entity->hasComponent<CAnimation>()) { continue; }

		auto animation = entity->getComponent<CAnimation>();

		//Handle player animation
		if (entity == m_player) {
			if (m_player->getComponent<CState>()->state == "standing" && animation->animation.getName() != "Stand") {
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("Stand"), true);
			}
			else if (m_player->getComponent<CState>()->state == "running" && animation->animation.getName() != "Run") {
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("Run"), true);
			}
			else if (m_player->getComponent<CState>()->state == "jumping" && animation->animation.getName() != "Air") {
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("Air"), true);
			}
			
		}

		//Update
		animation->animation.update();

		//Destroy entity if animation does not repeat and has finish
		if (animation->animation.hasEnded() && !animation->repeat) {
			entity->destroy();
		}
	}
}

void GameState_Play::sRender()
{
    //Color the background darker so you know that the game is paused
    if (!m_paused)  { m_game.window().clear(sf::Color(100, 100, 255)); }
    else            { m_game.window().clear(sf::Color(50, 50, 150)); }

    //Set the viewport of the window to be centered on the player if it's far enough right
    auto pPos = m_player->getComponent<CTransform>()->pos;
    float windowCenterX = fmax(m_game.window().getSize().x / 2.0f, pPos.x);
    sf::View view = m_game.window().getView();
    view.setCenter(windowCenterX, m_game.window().getSize().y - view.getCenter().y);
    m_game.window().setView(view);
        
    //Draw all Entity textures / animations
    if (m_drawTextures)
    {
        for (auto e : m_entityManager.getEntities())
        {
            auto transform = e->getComponent<CTransform>();

            if (e->hasComponent<CAnimation>())
            {
                auto animation = e->getComponent<CAnimation>()->animation;
                animation.getSprite().setRotation(transform->angle);
                animation.getSprite().setPosition(transform->pos.x, m_game.window().getSize().y - transform->pos.y);
                animation.getSprite().setScale(transform->scale.x, transform->scale.y);
                m_game.window().draw(animation.getSprite());
            }
        }
    }

    //Draw all Entity collision bounding boxes with a rectangleshape
    if (m_drawCollision)
    {
        for (auto e : m_entityManager.getEntities())
        {
            if (e->hasComponent<CBoundingBox>())
            {
                auto box = e->getComponent<CBoundingBox>();
                auto transform = e->getComponent<CTransform>();
                sf::RectangleShape rect;
                rect.setSize(sf::Vector2f(box->size.x-1, box->size.y-1));
                rect.setOrigin(sf::Vector2f(box->halfSize.x, box->halfSize.y));
                rect.setPosition(transform->pos.x, m_game.window().getSize().y - transform->pos.y);
                rect.setFillColor(sf::Color(0, 0, 0, 0));
                rect.setOutlineColor(sf::Color(255, 255, 255, 255));
                rect.setOutlineThickness(1);
                m_game.window().draw(rect);
            }
        }
    }

	//Render score
	m_scoreText.setString(std::to_string(m_player->getComponent<CScore>()->score));
	m_scoreText.setPosition(view.getCenter().x - floor((m_game.window().getSize().x) / 2.0f) + 10, 10);
	m_game.window().draw(m_scoreText);

    m_game.window().display();
}